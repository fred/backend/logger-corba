ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:


1.1.0 (2023-10-09)
------------------

* Implemented ``getRequestCountUsers``
* Syslog facility changed from 0..7 to LOG_LOCALx (x=0..7)
* Better handling of monitoring host file in configuration


1.0.0 (2020-09-18)
------------------

* Initial release
