cmake_minimum_required(VERSION 3.5)

find_program(IDL_PROGRAM omniidl)

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Specifies the build type. Possible values are empty, Debug, Release, RelWithDebInfo, MinSizeRel, ...")
set(CMAKE_CXX_FLAGS_RELEASE "-O2 -DNDEBUG")
option(THREADS_PREFER_PTHREAD_FLAG "The -pthread compiler and linker flag preference." TRUE)
set(SPDLOG_ACTIVE_LEVEL "SPDLOG_LEVEL_TRACE" CACHE STRING "This will turn on/off logging statements at compile time.")
set(FMT_STRING_ALIAS "1" CACHE STRING "Constructs a compile-time format string.")
set(FMT_HEADER_ONLY "1" CACHE STRING "Use fmt library in the header-only manner.")
option(CMAKE_EXPORT_COMPILE_COMMANDS "If enabled, generates a compile_commands.json file containing the exact compiler calls." ON)

macro(append_path output_variable_name)
    foreach(ARG ${ARGN})
        if(${output_variable_name})
            string(APPEND ${output_variable_name} "/${ARG}")
        else()
            set(${output_variable_name} "${ARG}")
        endif()
    endforeach()
    string(REGEX REPLACE "//+" "/" ${output_variable_name} "${${output_variable_name}}")
    string(REGEX REPLACE "(.)/$" "\\1" ${output_variable_name} "${${output_variable_name}}")
endmacro()

macro(concat_path output_variable_name)
    set(concat_path_tmp_variable "")
    append_path(concat_path_tmp_variable ${ARGN})
    set(${output_variable_name} "${concat_path_tmp_variable}" PARENT_SCOPE)
endmacro()

macro(set_default_path variable_name)
    if(NOT ${variable_name})
        set(${variable_name} "")
        append_path(${variable_name} ${ARGN})
    endif()
endmacro()

# project details

include("cmake/package.cmake")

package(fred-logger-corba
    FULL_VERSION "1.1.0")

project(FredLoggerCorba
    VERSION ${PACKAGE_VERSION}
    LANGUAGES CXX)

# build options

option(LOGGER_BUILD_TESTS "Build ${PACKAGE_NAME} tests" OFF)

# package dependencies

include("cmake/package_dependencies.cmake")

set(IDL_BUILD FALSE)
add_package_dependencies(FROM_FILE "dependencies.txt")
provide_package_dependency_dist(idl)

# dependencies
# Set CMake policies to support later version behaviour
set(CMAKE_POLICY_DEFAULT_CMP0077 NEW) # option() honors variables already set

# Make sure that custom modules like FindOMNIORB are found
set_default_path(IDL_PROJECT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external/idl)
set(IDL_SOURCE_DIR ${IDL_PROJECT_DIR}/idl)
#set(IDL_DESTINATION_DIR ${CMAKE_CURRENT_BINARY_DIR}/generated/corba)
list(INSERT CMAKE_MODULE_PATH 0 ${IDL_PROJECT_DIR}/cmake)

find_package(Threads REQUIRED)

set(CMAKE_FIND_PACKAGE_PREFER_CONFIG TRUE)
find_package(Boost 1.53.0
    COMPONENTS
        date_time
        program_options
        system
    REQUIRED)

# configuration files

set_default_path(DEFAULT_CONFIG_FILE "${CMAKE_INSTALL_FULL_SYSCONFDIR}" fred/fred-logger-corba.conf)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config.h)

function(set_common_properties_on_targets)
    foreach(target_name ${ARGN})
        message(STATUS "setting common properties on: " ${target_name})
        set_target_properties(${target_name} PROPERTIES
            CXX_STANDARD 14
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
        target_include_directories(${target_name}
            PUBLIC
                ${CMAKE_CURRENT_SOURCE_DIR}
                ${CMAKE_CURRENT_BINARY_DIR})
        target_compile_options(${target_name}
            PRIVATE
                $<$<CXX_COMPILER_ID:GNU>:-Wall -Wextra -ggdb -grecord-gcc-switches>)
    endforeach()
endfunction()

function(set_idl_properties_on_targets)
    foreach(target_name ${ARGN})
        message(STATUS "setting idl properties on: " ${target_name})
        target_compile_options(${target_name}
            PRIVATE
                "-Wno-unused-parameter"
                "-Wno-unused-variable")
    endforeach()
endfunction()

find_package(PkgConfig REQUIRED)

pkg_search_module(OMNIORB4 REQUIRED omniORB4>=4.1.2)
pkg_search_module(OMNIDYNAMIC4 REQUIRED omniDynamic4>=4.1.2)

include(CheckIncludeFileCXX)
function(abort_if_headers_not_found)
    foreach(header_name ${ARGN})
        check_include_file_cxx(${header_name} FOUND${header_name})
        if(NOT FOUND${header_name})
            message(FATAL_ERROR "Header ${header_name} not found!")
        endif()
    endforeach()
endfunction()

set(Resolv_LIBRARIES resolv)
set(Rt_LIBRARIES rt)

function(compile_idl_files)
    message(STATUS "Compiling idl...")
    if(NOT IDL_PROGRAM)
        message(FATAL_ERROR "omniidl not found")
    endif()
    execute_process(
        COMMAND mkdir -p src/corba
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR})
    foreach(idl_name ${ARGN})
        if(NOT EXISTS ${IDL_SOURCE_DIR}/${idl_name}.idl)
            message(FATAL_ERROR "${IDL_SOURCE_DIR}/${idl_name}.idl does not exist; you need to pass -DIDL_SOURCE_DIR")
        endif()
        execute_process(
            COMMAND ${IDL_PROGRAM} -bcxx -Wbuse_quotes -Wba -C ./src/corba ${IDL_SOURCE_DIR}/${idl_name}.idl
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
            OUTPUT_VARIABLE OMNIIDL_MESSAGE${idl_name})
        message(STATUS ${OMNIIDL_MESSAGE}${idl_name})
    endforeach()
    message(STATUS "...done")
endfunction()

add_library(logger-core STATIC
    src/args.cc
    src/cfg.cc
    src/corba_unwrap.cc
    src/corba_wrap.cc
    src/exceptions.cc
    src/get_dsn.cc
    src/is_monitoring_ip.cc
    src/log_entry_cache.cc
    src/log_entry_type_cache.cc
    src/nameservice.cc
    src/service_cache.cc
    src/session_cache.cc
    src/impl/close_log_entry.cc
    src/impl/close_session.cc
    src/impl/create_log_entry.cc
    src/impl/create_session.cc
    src/impl/get_log_entry_types.cc
    src/impl/get_object_reference_types.cc
    src/impl/get_request_count_users.cc
    src/impl/get_results.cc
    src/impl/get_services.cc
    src/util.cc
    src/legacy/corba_container.cc)

add_library(LoggerCore::library ALIAS logger-core)

add_library(fred-logger-corba-idl STATIC
    src/corba/LoggerSK.cc
    src/corba/LoggerDynSK.cc
    src/corba/AdminSK.cc
    src/corba/AdminDynSK.cc
    src/corba/RegistrySK.cc
    src/corba/RegistryDynSK.cc
    src/corba/FiltersSK.cc
    src/corba/FiltersDynSK.cc
    src/corba/_dataTypesSK.cc
    src/corba/_dataTypesDynSK.cc
    src/corba/DateTimeSK.cc
    src/corba/DateTimeDynSK.cc)

add_library(FredLoggerCorbaIdl::library ALIAS fred-logger-corba-idl)

target_link_libraries(fred-logger-corba-idl
    ${OMNIORB4_LIBRARIES}
    ${OMNIDYNAMIC4_LIBRARIES})

target_link_libraries(logger-core
    FredLoggerCorbaIdl::library
    Boost::program_options
    Threads::Threads
    LibLogger::library
    LibPg::library
    LibLog::library)

add_executable(fred-logger-corba
    src/iface/logger.cc
    src/main.cc)

target_link_libraries(fred-logger-corba PRIVATE
    LoggerCore::library)

set_common_properties_on_targets(
    fred-logger-corba-idl
    fred-logger-corba
    logger-core)

set_idl_properties_on_targets(
    fred-logger-corba-idl)

compile_idl_files(
    Logger
    Admin
    Registry
    Filters
    _dataTypes
    DateTime)

# tests

if(LOGGER_BUILD_TESTS)
    add_subdirectory(test)
endif()

# installation

include(GNUInstallDirs)

install(TARGETS ${PACKAGE_NAME}
    DESTINATION ${CMAKE_INSTALL_SBINDIR})

if(NOT TARGET uninstall)
    add_custom_target(uninstall
        COMMAND xargs -L10 rm -v < ${CMAKE_CURRENT_BINARY_DIR}/install_manifest.txt
        COMMAND cat ${CMAKE_CURRENT_BINARY_DIR}/install_manifest.txt | xargs -L1 dirname | sort | uniq | xargs -L10 rmdir -p --ignore-fail-on-non-empty)
endif()

# dist

include("cmake/package_dist.cmake")
package_dist(PACKAGE_NAME "${PACKAGE_NAME}"
             PACKAGE_TARNAME "${PACKAGE_TARNAME}")

list_package_dependencies_recursive("${PACKAGE_NAME}")
