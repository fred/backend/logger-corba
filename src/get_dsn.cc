/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/get_dsn.hh"

#include "src/cfg.hh"

namespace Fred {
namespace Logger {

LibPg::Dsn get_dsn()
{
    static LibPg::Dsn dsn;
    dsn.host = LibPg::Dsn::Host{Cfg::Options::get().database.host};
    if (Cfg::Options::get().database.host_addr != boost::none)
    {
        dsn.host_addr = LibPg::Dsn::HostAddr{boost::asio::ip::address::from_string(*Cfg::Options::get().database.host_addr)};
    }
    if (Cfg::Options::get().database.port != boost::none)
    {
        dsn.port = LibPg::Dsn::Port{std::uint16_t(*Cfg::Options::get().database.port)};
    }
    dsn.db_name = LibPg::Dsn::DbName{Cfg::Options::get().database.dbname};
    dsn.user = LibPg::Dsn::User{Cfg::Options::get().database.user};
    if (Cfg::Options::get().database.password != boost::none)
    {
        dsn.password = LibPg::Dsn::Password{*Cfg::Options::get().database.password};
    }
    dsn.connect_timeout = LibPg::Dsn::ConnectTimeout{std::chrono::seconds{*Cfg::Options::get().database.timeout}};
    //LIBLOG_INFO("DSN: {}@{}[{}]:{}/{}",
    //        Cfg::Options::get().database.user,
    //        Cfg::Options::get().database.host,
    //        Cfg::Options::get().database.host_addr.value_or(""),
    //        Cfg::Options::get().database.port.value_or(0),
    //        Cfg::Options::get().database.dbname);
    return dsn;
}

} // namespace Fred::Logger
} // namespace Fred
