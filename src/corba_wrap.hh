/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CORBA_WRAP_HH_21B4BAD2C849449F949C9A10BAFF65A2
#define CORBA_WRAP_HH_21B4BAD2C849449F949C9A10BAFF65A2

#include "src/corba/Logger.hh"

#include "src/impl/get_log_entry_types.hh"
#include "src/impl/get_request_count_users.hh"
#include "src/impl/get_services.hh"

#include "liblogger/create_log_entry.hh"
#include "liblogger/create_session.hh"
#include "liblogger/get_log_entry_info.hh"
#include "liblogger/get_object_reference_types.hh"
#include "liblogger/get_results.hh"

namespace Fred {
namespace Logger {

// createRequest
ccReg::TID corba_wrap(const LibLogger::CreateLogEntryReply& _reply);

// createSession
ccReg::TID corba_wrap(const LibLogger::CreateSessionReply& _reply);

// getRequestTypesByService
ccReg::RequestTypeList* corba_wrap(const Impl::GetLogEntryTypesReply& _reply);

// getServices
ccReg::RequestServiceList* corba_wrap(const Impl::GetServicesReply& _reply);

// getResultCodesByService
ccReg::ResultCodeList* corba_wrap(const LibLogger::GetResultsReply& _reply);

// getObjectTypes
ccReg::Logger::ObjectTypeList* corba_wrap(const LibLogger::GetObjectReferenceTypesReply& _reply);

// getRequestCountUsers
ccReg::RequestCountInfo* corba_wrap(const Impl::RequestCountInfo& _reply);

} // namespace Fred::Logger
} // namespace Fred

#endif
