/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CFG_HH_60719D368658452AB0C1F829B8F041F9
#define CFG_HH_60719D368658452AB0C1F829B8F041F9

#include "liblog/level.hh"

#include "src/args.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/blank.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>
#include <boost/variant.hpp>

#include <stdexcept>
#include <string>
#include <vector>

namespace Cfg {

class AllDone : public std::exception
{
public:
    explicit AllDone(const std::string& msg);
    const char* what()const noexcept override;
private:
    const std::string msg_;
};

struct Exception : std::runtime_error
{
    explicit Exception(const std::string& msg);
};

struct UnknownOption : Exception
{
    explicit UnknownOption(const std::string& msg);
};

struct MissingOption : Exception
{
    explicit MissingOption(const std::string& msg);
};

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int argc, const char* const* argv);

    boost::optional<std::string> config_file_name;
    struct Nameservice
    {
        std::string host;
        unsigned int port;
        std::string context;
        Fred::Logger::Args orb;
        std::string service_logger;
        std::string service_logger_request_count;
    } nameservice;
    struct Database
    {
        std::string host;
        boost::optional<std::string> host_addr;
        boost::optional<int> port;
        std::string user;
        std::string dbname;
        boost::optional<std::string> password;
        boost::optional<int> timeout;
    } database;
    struct Log
    {
        struct Console
        {
            boost::optional<LibLog::Level> min_severity;
        };
        struct Logfile
        {
            std::string file_name;
            boost::optional<LibLog::Level> min_severity;
        };
        struct Syslog
        {
            int facility;
            boost::optional<LibLog::Level> min_severity;
        };
        boost::variant<boost::blank, Console, Logfile, Syslog> device;
    } log;
    struct Logger
    {
        std::string monitoring_hosts_file;
        std::vector<boost::asio::ip::address> monitoring_hosts_ips;
        std::size_t log_entry_cache_size_max;
        std::size_t session_cache_size_max;
    } logger;
private:
    Options(int argc, const char* const* argv);
};

}//namespace Cfg

#endif
