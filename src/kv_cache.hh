/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KV_CACHE_HH_2855733A6CF54EE99E8878DAC2A6345B
#define KV_CACHE_HH_2855733A6CF54EE99E8878DAC2A6345B

#include "src/hash_strong_type.hh"

#include <libpg/util/strong_type_operators.hh>

#include <boost/optional.hpp>

#include <list>
#include <mutex>
#include <tuple>
#include <unordered_map>

namespace Fred {
namespace Logger {

template <typename K, typename V, std::size_t cache_size_max>
class KvCache
{
public:
    using Key = K;
    using Value = V;

    static KvCache& get_instance()
    {
        static KvCache kv_cache{};
        return kv_cache;
    }

    void push(const K& _key, const V& _value)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        while (cache_.size() >= cache_size_max_)
        {
            const auto last_key = std::get<K>(cache_.back());
            // this is why the key is also cached
            items_.erase(last_key);
            cache_.pop_back();
        }

        cache_.push_front(std::make_tuple(_key, _value));
        items_[_key] = cache_.begin();
    }

    template <typename OnCacheMissDbTransaction>
    V get(const K& _key, OnCacheMissDbTransaction&& _on_cache_miss_db_transaction)
    {
        {
            const auto cached_v = get(_key);
            if (cached_v != boost::none)
            {
                return *cached_v;
            }
        }
        std::lock_guard<std::mutex> lock_guard(on_cache_miss_mutex_);
        { 
            const auto cached_v = get(_key);
            if (cached_v != boost::none)
            {
                return *cached_v;
            }
        }
        const auto v = get_on_cache_miss(std::forward<OnCacheMissDbTransaction>(_on_cache_miss_db_transaction)(), _key);
        push(_key, v);
        return v;
    }

    void erase(const K& _key)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        const auto map_item = items_.find(_key);
        if (map_item != items_.end())
        {
            const auto cache_item = map_item->second;
            cache_.erase(cache_item);
            items_.erase(map_item);
        }
    }

    void set_cache_size_max(const std::size_t _cache_size_max)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        cache_size_max_ = _cache_size_max;
    }

private:
    KvCache()
        : cache_size_max_(cache_size_max)
    {
    }

    boost::optional<V> get(const K& _key)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        const auto map_item = items_.find(_key);
        const auto cache_hit = map_item != items_.end();
        return cache_hit ? boost::optional<V>{std::get<V>(*map_item->second)} : boost::none;
    }

    template <typename DbTransaction>
    V get_on_cache_miss(const DbTransaction& _db_transaction, const K& _key);

    using Queue = std::list<std::tuple<K, V>>;
    using Map = std::unordered_map<K, typename Queue::iterator>;
    Queue cache_;
    Map items_;
    std::size_t cache_size_max_;
    std::mutex cache_mutex_;
    std::mutex on_cache_miss_mutex_;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
