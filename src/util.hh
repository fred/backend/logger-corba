/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_HH_1133CF82BC874C8FAAE4694AE2C31AD0
#define UTIL_HH_1133CF82BC874C8FAAE4694AE2C31AD0

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include "libpg/pg_transaction.hh"

namespace Fred {
namespace Logger {

LibLogger::SessionIdent get_session_ident_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::SessionId& _session_id);

LibLogger::LogEntryIdent get_log_entry_ident_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryId& _log_entry_id);

LibLogger::LogEntry::ServiceName get_service_name_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::ServiceId& _service_id);

LibLogger::LogEntry::ServiceId get_service_id_by_name(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::ServiceName& _service_name);

LibLogger::LogEntry::LogEntryTypeName get_log_entry_type_name_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryTypeId& _log_entry_type_id);

LibLogger::LogEntry::LogEntryTypeId get_log_entry_type_id_by_name(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryTypeName& _log_entry_type_name);

} // namespace Fred::Logger
} // namespace Fred

#endif
