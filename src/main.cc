/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg.hh"
#include "src/legacy/corba_container.hh"

#include "src/log_entry_cache.hh"
#include "src/session_cache.hh"

#include "src/iface/logger.hh"

#include "liblog/liblog.hh"
#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include "libpg/detail/libpq_layer_impl.hh"
#include "libpg/libpq_layer.hh"
#include "libpg/pg_connection.hh"

#include <boost/assign/list_of.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>

#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>

namespace Fred {
namespace Logger {

namespace {

enum class LogHandler
{
    is_set,
    is_not_set
};

class SetLogHandler : public boost::static_visitor<LogHandler>
{
public:
    explicit SetLogHandler(LibLog::LogConfig& liblog_log_config)
        : liblog_log_config_{liblog_log_config}
    {
    }
    LogHandler operator()(const boost::blank&)const
    {
        return LogHandler::is_not_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Console& option)const
    {
        auto console_sink_config =
                LibLog::Sink::ConsoleSinkConfig()
                        .set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        if (option.min_severity != boost::none)
        {
            console_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(console_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Logfile& option)const
    {
        auto file_sink_config =
                LibLog::Sink::FileSinkConfig(option.file_name);
        if (option.min_severity != boost::none)
        {
            file_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(file_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Syslog& option)const
    {
        auto syslog_sink_config =
                LibLog::Sink::SyslogSinkConfig()
                        .set_syslog_facility(option.facility);
        if (option.min_severity != boost::none)
        {
            syslog_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(syslog_sink_config);
        return LogHandler::is_set;
    }
private:
    LibLog::LogConfig& liblog_log_config_;
};

void show_query_argument_value(std::ostream& out, int idx, const char* value)
{
    if (idx != 0)
    {
        out << ", ";
    }
    out << "$" << (idx + 1) << ":";
    if (value == nullptr)
    {
        out << "NULL";
    }
    else
    {
        out << "\"" << value << "\"";
    }
}

auto values_to_string(const char* const* values, int number_of_values)
{
    std::ostringstream out;
    out << "[";
    for (int idx = 0; idx < number_of_values; ++idx)
    {
        show_query_argument_value(out, idx, values[idx]);
    }
    out << "]";
    return std::move(out).str();
}

class LibPgLogging
{
public:
    LibPgLogging()
    {
        LibPg::set_libpq_layer(static_cast<LibPg::LibPqLayer*>(&my_libpq_layer_impl_));
    }
    ~LibPgLogging()
    {
        LibPg::set_libpq_layer(nullptr);
    }
private:
    class LibPq : public LibPg::Detail::LibPqLayerSimpleImplementation
    {
    public:
        LibPq()
        {
        }
        ~LibPq()override
        {
        }
    private:
        using Parent = LibPg::Detail::LibPqLayerSimpleImplementation;
        ::PGconn* PQconnectdbParams(
                const char* const* keywords,
                const char* const* values,
                int expand_dbname) const override
        {
            auto* const conn = this->Parent::PQconnectdbParams(keywords, values, expand_dbname);
            this->PQsetNoticeProcessor(
                    conn,
                    [](void*, const char* message) {
                        // notice|warning messages from PostgreSQL
                        LIBLOG_WARNING((std::string{message, std::strlen(message) - 1}));
                    },
                    nullptr);
            return conn;
        }
        ::PGresult* PQexec(::PGconn* conn, const char* query)const override
        {
            LIBLOG_DEBUG("query: {}", query);
            return this->Parent::PQexec(conn, query);
        }
        ::PGresult* PQexecParams(
                ::PGconn* conn,
                const char* command,
                int nParams,
                const ::Oid* paramTypes,
                const char* const* paramValues,
                const int* paramLengths,
                const int* paramFormats,
                int resultFormat) const override
        {
            LIBLOG_DEBUG("query: {} {}", command, values_to_string(paramValues, nParams));
            return this->Parent::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
        }
    };
    LibPq my_libpq_layer_impl_;
};

void run_server(CorbaContainer* corba_instance_ptr)
{
    corba_instance_ptr->poa_persistent->the_POAManager()->activate();
    corba_instance_ptr->orb->run();
}

void corba_init(const Cfg::Options::Nameservice& option)
{
    LIBLOG_INFO("Registering to CORBA Name Service at {}:{}, context: {}", option.host, option.port, option.context);
    CorbaContainer::set_instance(
            option.orb.argc(),
            option.orb.argv(),
            option.host,
            option.port,
            option.context);
}

} // namespace Fred::Logger::{anonymous}
} // namespace Fred::Logger
} // namespace Fred

int main(int argc, char *argv[])
{
    try
    {
        Cfg::Options::init(argc, argv);

        Fred::Logger::SessionCache::get_instance().set_cache_size_max(Cfg::Options::get().logger.session_cache_size_max);
        Fred::Logger::LogEntryCache::get_instance().set_cache_size_max(Cfg::Options::get().logger.log_entry_cache_size_max);
    }
    catch (const Cfg::AllDone& all_done)
    {
        std::cout << all_done.what() << std::endl;
        return EXIT_SUCCESS;
    }
    catch (const Cfg::UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }

    constexpr LibLog::ThreadMode threading = LibLog::ThreadMode::multi_threaded; // global, compile-time parameter
    std::unique_ptr<Fred::Logger::LibPgLogging> libpq_log_layer;
    auto liblog_log_config = LibLog::LogConfig{};
    switch (boost::apply_visitor(Fred::Logger::SetLogHandler{liblog_log_config}, Cfg::Options::get().log.device))
    {
        case Fred::Logger::LogHandler::is_set:
            LibLog::Log::start<threading>(liblog_log_config);
            libpq_log_layer = std::make_unique<Fred::Logger::LibPgLogging>();
            LIBLOG_DEBUG("fred-logger-services configuration successfully loaded (log configured)");
            break;
        case Fred::Logger::LogHandler::is_not_set:
            std::cout << "fred-logger-services configuration successfully loaded (log not configured)" << std::endl;
            break;
    }

    try
    {
        Fred::Logger::corba_init(Cfg::Options::get().nameservice);
        std::unique_ptr<Fred::Logger::Iface::LoggerService> myccReg_Logger_i(new Fred::Logger::Iface::LoggerService());
        CorbaContainer::get_instance()->register_server(myccReg_Logger_i.release(), Cfg::Options::get().nameservice.service_logger);
        Fred::Logger::run_server(CorbaContainer::get_instance());
        LIBLOG_INFO("Server shutdown");
        return EXIT_SUCCESS;
    }
    catch (const CORBA::TRANSIENT&)
    {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the server." << std::endl;
        return EXIT_FAILURE;
    }
    catch (const CORBA::SystemException& e)
    {
        std::cerr << "Caught a CORBA::" << e._name() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const CORBA::Exception& e)
    {
        std::cerr << "Caught CORBA::Exception: " << e._name() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const omniORB::fatalException& e)
    {
        std::cerr << "Caught omniORB::fatalException:" << std::endl;
        std::cerr << "  file: " << e.file() << std::endl;
        std::cerr << "  line: " << e.line() << std::endl;
        std::cerr << "  mesg: " << e.errmsg() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}
