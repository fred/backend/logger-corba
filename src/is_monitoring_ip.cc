/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/is_monitoring_ip.hh"

#include "src/cfg.hh"

#include <algorithm>

namespace Fred {
namespace Logger {

bool is_monitoring_ip(const boost::asio::ip::address& _ip)
{
    const auto& monitoring_ips = Cfg::Options::get().logger.monitoring_hosts_ips;
    return std::find(monitoring_ips.cbegin(), monitoring_ips.cend(), _ip) != monitoring_ips.cend();
}

} // namespace Fred::Logger
} // namespace Fred
