/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/log_entry_cache.hh"

#include "src/cfg.hh"
#include "src/exceptions.hh"

#include "libpg/query.hh"

#include "liblog/liblog.hh"

namespace Fred {
namespace Logger {

namespace {

std::tuple<LibLogger::LogEntry::TimeBegin, LibLogger::LogEntry::ServiceName, LibLogger::LogEntry::IsMonitoring>
get_log_entry_details_by_id(const LibPg::PgTransaction& _transaction, const LibLogger::LogEntry::LogEntryId& _log_entry_id)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LibLogger::LogEntry::TimeBegin>() << "r.time_begin"
                     << LibPg::item<LibLogger::LogEntry::ServiceName>() << "s.name"
                     << LibPg::item<LibLogger::LogEntry::IsMonitoring>() << "r.is_monitoring "
              "FROM request r "
              "JOIN service s "
                "ON s.id = r.service_id "
             "WHERE r.id = " << LibPg::parameter<LibLogger::LogEntry::LogEntryId>().as_big_int();
            // clang-format on

    const auto db_result = exec(_transaction, query, _log_entry_id);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("log_entry \"{}\" not found", _log_entry_id.get());
        throw LogEntryDoesNotExist{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto time_begin = db_result.front().get<LibLogger::LogEntry::TimeBegin>();
    const auto service_name = db_result.front().get<LibLogger::LogEntry::ServiceName>();
    const auto monitoring_status = db_result.front().get<LibLogger::LogEntry::IsMonitoring>();

    return std::make_tuple(time_begin, service_name, monitoring_status);
}

} // namespace Fred::Logger::{anonymous}

template<> template<>
LogEntryCache::Value LogEntryCache::get_on_cache_miss(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryId& _log_entry_id)
{
    LIBLOG_DEBUG("partitioning related details of log_entry of id \"{}\" not cached, querying db", _log_entry_id.get());

    const auto log_entry_details = get_log_entry_details_by_id(_transaction, _log_entry_id);

    return std::make_tuple(
            std::get<LibLogger::LogEntry::TimeBegin>(log_entry_details),
            std::get<LibLogger::LogEntry::ServiceName>(log_entry_details),
            std::get<LibLogger::LogEntry::IsMonitoring>(log_entry_details));
}

} // namespace Fred::Logger
} // namespace Fred
