/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CORBA_UNWRAP_HH_05B87560B66444DBA8D33AC650EE0D5A
#define CORBA_UNWRAP_HH_05B87560B66444DBA8D33AC650EE0D5A

#include "src/corba/Logger.hh"
#include "src/corba/_dataTypes.hh"

#include "src/impl/get_request_count_users.hh"

#include "liblogger/log_entry.hh"
#include "liblogger/session_ident.hh"

namespace Fred {
namespace Logger {

template<typename T>
T corba_unwrap(const char* _src);

template <typename T>
T corba_unwrap(CORBA::Long _src);

template <typename T>
T corba_unwrap(ccReg::TID _src);

// createRequest
// const char *source_ip,
// ccReg::RequestServiceType service,
// const char *content,
// const ccReg::RequestProperties& props,
// const ccReg::ObjectReferences &refs,
// CORBA::Long request_type_id,
// ccReg::TID session_id

template<>
boost::optional<LibLogger::LogEntry::SourceIp> corba_unwrap<boost::optional<LibLogger::LogEntry::SourceIp>>(const char* _src);

LibLogger::LogEntry::ServiceId corba_unwrap(const ccReg::RequestServiceType& _src);

template<>
LibLogger::LogEntry::Content corba_unwrap<LibLogger::LogEntry::Content>(const char* _src);

std::vector<LibLogger::LogEntryProperty> corba_unwrap(const ccReg::RequestProperties& _request_properties);

std::vector<LibLogger::ObjectReferences> corba_unwrap(const ccReg::ObjectReferences& _object_references);

template <>
LibLogger::LogEntry::LogEntryTypeId corba_unwrap<LibLogger::LogEntry::LogEntryTypeId>(CORBA::Long _src);

// closeRequest
// ccReg::TID id,
// const char *content,
// const ccReg::RequestProperties &props,
// const ccReg::ObjectReferences &refs,
// const CORBA::Long result_code,
// ccReg::TID session_id

template <>
LibLogger::LogEntry::LogEntryId corba_unwrap<LibLogger::LogEntry::LogEntryId>(ccReg::TID _src);

template <>
LibLogger::LogEntry::ResultCode corba_unwrap<LibLogger::LogEntry::ResultCode>(CORBA::Long _src);

// createSession
// ccReg::TID user_id,
// const char *name

template <>
boost::optional<LibLogger::LogEntry::UserId> corba_unwrap<boost::optional<LibLogger::LogEntry::UserId>>(ccReg::TID _src);

template<>
LibLogger::LogEntry::UserName corba_unwrap<LibLogger::LogEntry::UserName>(const char* _src);

// closeSession
// ccReg::TID id

template <>
LibLogger::LogEntry::SessionId corba_unwrap<LibLogger::LogEntry::SessionId>(ccReg::TID _src);

template <>
boost::optional<LibLogger::LogEntry::SessionId> corba_unwrap<boost::optional<LibLogger::LogEntry::SessionId>>(ccReg::TID _src);

// getRequestTypesByService
// ccReg::RequestServiceType service

// getResultCodesByService
// ccReg::RequestServiceType service

// getRequestCountUsers
// const char *datetime_from
// const char *datetime_to
// const char *service

template<>
Impl::RequestCountFrom corba_unwrap<Impl::RequestCountFrom>(const char* _src);

template<>
Impl::RequestCountTo corba_unwrap<Impl::RequestCountTo>(const char* _src);

template<>
LibLogger::LogEntry::ServiceName corba_unwrap<LibLogger::LogEntry::ServiceName>(const char* _src);

} // namespace Fred::Logger
} // namespace Fred

#endif
