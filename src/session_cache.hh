/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SESSION_CACHE_HH_6CDB0074508D48619DE50602CD1F4342
#define SESSION_CACHE_HH_6CDB0074508D48619DE50602CD1F4342

#include "src/hash_strong_type.hh"
#include "src/kv_cache.hh"

#include "liblogger/log_entry.hh"

#include "libpg/util/strong_type_operators.hh"

namespace Fred {
namespace Logger {

using SessionCache = class KvCache<LibLogger::LogEntry::SessionId, LibLogger::LogEntry::SessionLoginDate, 1000>;

} // namespace Fred::Logger
} // namespace Fred

#endif
