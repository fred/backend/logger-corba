/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/log_entry_type_cache.hh"

#include "libpg/pg_transaction.hh"
#include "libpg/query.hh"

#include "liblog/liblog.hh"

namespace Fred {
namespace Logger {

template<> template<>
void LogEntryTypeCache::reload(
        const LibPg::PgTransaction& _transaction)
{
    LIBLOG_DEBUG("log_entry_types (re)loaded");
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LibLogger::LogEntry::LogEntryTypeName>() << "name" <<
                        LibPg::item<LibLogger::LogEntry::LogEntryTypeId>() << "id "
              "FROM request_type";
            // clang-format on

    const auto db_result = exec(_transaction, query);

    items_.by_u.clear();
    items_.by_v.clear();
    for (LibPg::RowIndex idx = LibPg::RowIndex{0}; idx < db_result.size(); ++idx)
    {
        items_.by_u[db_result[idx].get<LibLogger::LogEntry::LogEntryTypeId>()] = db_result[idx].get<LibLogger::LogEntry::LogEntryTypeName>(); 
        items_.by_v[db_result[idx].get<LibLogger::LogEntry::LogEntryTypeName>()] = db_result[idx].get<LibLogger::LogEntry::LogEntryTypeId>(); 
    }
}

} // namespace Fred::Logger
} // namespace Fred
