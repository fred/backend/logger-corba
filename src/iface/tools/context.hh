/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTEXT_HH_CC26F747324D4D69B4B9EBD2BA3376F6
#define CONTEXT_HH_CC26F747324D4D69B4B9EBD2BA3376F6

#include "liblog/liblog.hh"

#define LOGGER_CORBA_REQUEST_CONTEXT() \
struct RequestContext \
{ \
    LibLog::SetContext ctx; \
    RequestContext(std::initializer_list<std::string> _context_hierarchy) : ctx{_context_hierarchy} \
    { \
        LIBLOG_INFO("begin"); \
    } \
    ~RequestContext() \
    { \
        try \
        { \
            LIBLOG_INFO("end"); \
        } \
        catch (...) { } \
    } \
} request_context{__func__}

#endif
