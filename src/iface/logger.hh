/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_HH_9E40A673DE7F466EACE088CAA028DA0D
#define LOGGER_HH_9E40A673DE7F466EACE088CAA028DA0D

#include "src/corba/Logger.hh"

namespace Fred {
namespace Logger {
namespace Iface {

class LoggerService final : public POA_ccReg::Logger
{

public:
    ccReg::TID createRequest(
            const char* _source_ip,
            ccReg::RequestServiceType _service,
            const char* _content,
            const ccReg::RequestProperties& _properties,
            const ccReg::ObjectReferences& _references,
            CORBA::Long _request_type_id,
            ccReg::TID _session_id) override;

    void closeRequest(
            ccReg::TID _id,
            const char* _content,
            const ccReg::RequestProperties& _properties,
            const ccReg::ObjectReferences& _references,
            const CORBA::Long _result_code,
            ccReg::TID _session_id) override;

    ccReg::TID createSession(
            ccReg::TID _user_id,
            const char* _user_name) override;

    void closeSession(
            ccReg::TID _id) override;

    ccReg::RequestTypeList* getRequestTypesByService(
            ccReg::RequestServiceType _service) override;

    ccReg::RequestServiceList* getServices() override;

    ccReg::ResultCodeList* getResultCodesByService(
            ccReg::RequestServiceType _service) override;

    ccReg::Logger::ObjectTypeList* getObjectTypes() override;

    ccReg::RequestCountInfo* getRequestCountUsers(
            const char* datetime_from,
            const char* datetime_to,
            const char* service) override;
};

} // namespace Fred::Logger::Iface
} // namespace Fred::Logger
} // namespace Fred

#endif
