/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/iface/logger.hh"

#include "src/iface/tools/context.hh"

#include "src/corba_unwrap.hh"
#include "src/corba_wrap.hh"
#include "src/exceptions.hh"

#include "src/iface/tools/context.hh"
#include "src/impl/close_log_entry.hh"
#include "src/impl/close_session.hh"
#include "src/impl/create_log_entry.hh"
#include "src/impl/create_session.hh"
#include "src/impl/get_log_entry_types.hh"
#include "src/impl/get_object_reference_types.hh"
#include "src/impl/get_request_count_users.hh"
#include "src/impl/get_results.hh"
#include "src/impl/get_services.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/log_entry.hh"

#include "liblog/liblog.hh"

namespace Fred {
namespace Logger {
namespace Iface {

ccReg::TID LoggerService::createRequest(
        const char* _source_ip,
        ccReg::RequestServiceType _service,
        const char* _content,
        const ccReg::RequestProperties& _properties,
        const ccReg::ObjectReferences& _references,
        CORBA::Long _request_type_id,
        ccReg::TID _session_id)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::create_log_entry(
                        corba_unwrap<boost::optional<LibLogger::LogEntry::SourceIp>>(_source_ip),
                        corba_unwrap(_service),
                        corba_unwrap<LibLogger::LogEntry::Content>(_content),
                        corba_unwrap(_properties),
                        corba_unwrap(_references),
                        corba_unwrap<LibLogger::LogEntry::LogEntryTypeId>(_request_type_id),
                        corba_unwrap<boost::optional<LibLogger::LogEntry::SessionId>>(_session_id));
        return corba_wrap(result);
    }
    catch (const InvalidIpAddress& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (const LibLogger::InvalidSessionIdent& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (const LibLogger::InvalidPropertyType& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::InvalidService& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ServiceNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::LogEntryTypeNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ObjectTypeNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::SessionDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

void LoggerService::closeRequest(
        ccReg::TID _id,
        const char* _content,
        const ccReg::RequestProperties& _properties,
        const ccReg::ObjectReferences& _references,
        const CORBA::Long _result_code,
        ccReg::TID _session_id)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        Impl::close_log_entry(
                corba_unwrap<LibLogger::LogEntry::LogEntryId>(_id),
                corba_unwrap<LibLogger::LogEntry::Content>(_content),
                corba_unwrap(_properties),
                corba_unwrap(_references),
                corba_unwrap<LibLogger::LogEntry::ResultCode>(_result_code),
                corba_unwrap<boost::optional<LibLogger::LogEntry::SessionId>>(_session_id));
    }
    catch (const SessionDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LogEntryDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::REQUEST_NOT_EXISTS{};
    }
    catch (const LibLogger::InvalidSessionIdent& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::InvalidLogEntryIdent& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::InvalidPropertyType& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::LogEntryTypeNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ServiceNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ObjectTypeNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ResultCodeNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::SessionDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::LogEntryDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::REQUEST_NOT_EXISTS{};
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::TID LoggerService::createSession(
        ccReg::TID _user_id,
        const char* _user_name)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::create_session(
                        corba_unwrap<LibLogger::LogEntry::UserName>(_user_name),
                        corba_unwrap<boost::optional<LibLogger::LogEntry::UserId>>(_user_id));
        return corba_wrap(result);
    }
    catch (const LibLogger::InvalidUserName& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

void LoggerService::closeSession(
        ccReg::TID _id)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        Impl::close_session(
                corba_unwrap<LibLogger::LogEntry::SessionId>(_id));
    }
    catch (const SessionDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::SESSION_NOT_EXISTS{};
    }
    catch (const LibLogger::InvalidSessionIdent& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::SessionAlreadyClosed& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::SessionDoesNotExist& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::SESSION_NOT_EXISTS{};
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::RequestTypeList* LoggerService::getRequestTypesByService(
        ccReg::RequestServiceType _service)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::get_log_entry_types(
                        corba_unwrap(_service));
        return corba_wrap(result);
    }
    catch (const LibLogger::InvalidService& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ServiceNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::RequestServiceList* LoggerService::getServices()
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::get_services();
        return corba_wrap(result);
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::ResultCodeList* LoggerService::getResultCodesByService(
        ccReg::RequestServiceType _service)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::get_results(
                        corba_unwrap(_service));
        return corba_wrap(result);
    }
    catch (const LibLogger::InvalidService& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const LibLogger::ServiceNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR();
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::Logger::ObjectTypeList* LoggerService::getObjectTypes()
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::get_object_reference_types();
        return corba_wrap(result);
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

ccReg::RequestCountInfo* LoggerService::getRequestCountUsers(
        const char* _datetime_from,
        const char* _datetime_to,
        const char* _service)
{
    LOGGER_CORBA_REQUEST_CONTEXT();
    try
    {
        const auto result =
                Impl::get_request_count_users(
                        corba_unwrap<Impl::RequestCountFrom>(_datetime_from),
                        corba_unwrap<Impl::RequestCountTo>(_datetime_to),
                        corba_unwrap<LibLogger::LogEntry::ServiceName>(_service));
        return corba_wrap(result);
    }
    catch (const InvalidInterval& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INCORRECT_USAGE{};
    }
    catch (const LibLogger::InvalidService& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INCORRECT_USAGE{};
    }
    catch (const LibLogger::ServiceNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INCORRECT_USAGE{};
    }
    catch (const RequestPropertyNameNotFound& e)
    {
        LIBLOG_INFO("{}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{}; // hardcoded "handle" not found
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
    catch (...)
    {
        LIBLOG_ERROR("server caught unexpected exception");
        throw ccReg::Logger::INTERNAL_SERVER_ERROR{};
    }
}

} // namespace Fred::Logger::Iface
} // namespace Fred::Logger
} // namespace Fred
