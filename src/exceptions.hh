/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXCEPTIONS_HH_0336AE72BB4540469F8FDF24FC34E460
#define EXCEPTIONS_HH_0336AE72BB4540469F8FDF24FC34E460

#include <exception>

namespace Fred {
namespace Logger {

struct InvalidIpAddress : std::exception
{
    const char* what() const noexcept override;
};

struct SessionDoesNotExist : std::exception
{
    const char* what() const noexcept override;
};

struct LogEntryDoesNotExist : std::exception
{
    const char* what() const noexcept override;
};

struct RequestPropertyNameNotFound : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidInterval : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidService : std::exception
{
    const char* what() const noexcept override;
};

struct ItemNotFound : std::exception
{
    const char* what() const noexcept override;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
