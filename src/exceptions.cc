/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/exceptions.hh"

namespace Fred {
namespace Logger {

const char* InvalidIpAddress::what() const noexcept
{
    return "invalid ip address";
}

const char* SessionDoesNotExist::what() const noexcept
{
    return "session does not exist";
}

const char* LogEntryDoesNotExist::what() const noexcept
{
    return "log entry does not exist";
}

const char* RequestPropertyNameNotFound::what() const noexcept
{
    return "request property name not found";
}

const char* InvalidInterval::what() const noexcept
{
    return "invalid interval";
}

const char* InvalidService::what() const noexcept
{
    return "invalid service";
}

const char* ItemNotFound::what() const noexcept
{
    return "item not found";
}

} // namespace Fred::Logger
} // namespace Fred
