/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ENUM_CACHE_HH_4EA22B61219146B1B6E0C10D2C813A07
#define ENUM_CACHE_HH_4EA22B61219146B1B6E0C10D2C813A07

#include "src/exceptions.hh"

#include <boost/optional.hpp>

#include <mutex>
#include <unordered_map>

namespace Fred {
namespace Logger {

template <typename U, typename V>
class UvCache
{
public:
    static UvCache& get_instance()
    {
        static UvCache uv_cache{};
        return uv_cache;
    }

    template <typename OnCacheMissDbTransaction>
    V get(const U& u_, OnCacheMissDbTransaction&& _on_cache_miss_db_transaction)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        {
            const auto cached_v = get(u_);
            if (cached_v != boost::none)
            {
                return *cached_v;
            }
        }
        reload(std::forward<OnCacheMissDbTransaction>(_on_cache_miss_db_transaction)());
        const auto v = get(u_);
        return v != boost::none ? *v : throw ItemNotFound{};
    }

    template <typename OnCacheMissDbTransaction>
    U get(const V& v_, OnCacheMissDbTransaction&& _on_cache_miss_db_transaction)
    {
        std::lock_guard<std::mutex> lock_guard(cache_mutex_);
        {
            const auto cached_u = get(v_);
            if (cached_u != boost::none)
            {
                return *cached_u;
            }
        }
        reload(std::forward<OnCacheMissDbTransaction>(_on_cache_miss_db_transaction)());
        const auto u = get(v_);
        return u != boost::none ? *u : throw ItemNotFound{};
    }

private:
    UvCache()
    {
    }

    boost::optional<V> get(const U& u_)
    {
        const auto v = items_.by_u.find(u_);
        const auto cache_hit = v != items_.by_u.end(); 
        return cache_hit ? boost::optional<V>{v->second} : boost::none;
    }

    boost::optional<U> get(const V& v_)
    {
        const auto u = items_.by_v.find(v_);
        const auto cache_hit = u != items_.by_v.end(); 
        return cache_hit ? boost::optional<U>{u->second} : boost::none;
    }

    template <typename DbTransaction>
    void reload(const DbTransaction& _transaction);

    struct Items
    {
        std::unordered_map<U, V> by_u;
        std::unordered_map<V, U> by_v;
    } items_;

    std::mutex cache_mutex_;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
