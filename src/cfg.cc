/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/cfg.hh"

#include "liblog/level.hh"

#include <syslog.h>

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "logger.conf"
#endif

namespace Cfg {

namespace {

LibLog::Level make_severity(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::Level> str2level =
    {
        { "critical", LibLog::Level::critical },
        { "error", LibLog::Level::error },
        { "warning", LibLog::Level::warning },
        { "info", LibLog::Level::info },
        { "debug", LibLog::Level::debug },
        { "trace", LibLog::Level::trace }
    };
    const auto level_itr = str2level.find(str);
    if (level_itr != str2level.end())
    {
        return level_itr->second;
    }
    throw Exception("\"" + str + "\" is not a valid severity");
}

void required_one_of(const boost::program_options::variables_map& variables_map, const char* variable)
{
    if (variables_map.count(variable) == 0)
    {
        throw MissingOption{"missing one of options: '" + std::string{variable} + "'"};
    }
}

template <typename ...Ts>
void required_one_of(
        const boost::program_options::variables_map& variables_map,
        const char* first_variable,
        const Ts* ...other_variables)
{
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    if (has_option(first_variable))
    {
        return;
    }
    try
    {
        required_one_of(variables_map, other_variables...);
    }
    catch (const MissingOption& e)
    {
        throw MissingOption{e.what() + std::string{", '"} + first_variable + "'"};
    }
}

std::vector<boost::asio::ip::address> get_monitoring_ips_from_file(const std::string& _filename)
{
    std::cout << "loading monitoring IPs from file: " << _filename << std::endl;
    std::vector<boost::asio::ip::address> monitoring_ips;
    std::ifstream infile{_filename};
    if (infile.fail())
    {
        throw Exception{"can not open file \"" + _filename + "\""};
    }
    std::string ipstr;
    while (infile >> ipstr)
    {
        boost::system::error_code boost_error_code;
        const auto ip = boost::asio::ip::address::from_string(ipstr, boost_error_code);
        if (boost_error_code)
        {
            std::cerr << "INVALID monitoring ip: " << ipstr << std::endl;
        }
        monitoring_ips.emplace_back(ip);
        std::cout << "monitoring ip: " << ipstr << " (imported as " << ip.to_string() << ")" << std::endl;
    }
    return monitoring_ips;
}

constexpr int invalid_argc = -1;
constexpr const char* const invalid_argv[0] = { };

} // namespace Cfg::{anonymous}

const Options& Options::get()
{
    return init(invalid_argc, invalid_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != invalid_argc) || (argv != invalid_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error{"First call of Cfg::Options::init must contain valid arguments"};
        }
        static const Options singleton(argc, argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error{"Only first call of Cfg::Options::init can contain valid arguments"};
    }
    return *singleton_ptr;
}

Options::Options(int argc, const char* const* argv)
{
    boost::program_options::options_description command_line_only_options("Generic options (only available on command line)");
    std::string opt_config_file_name;
    command_line_only_options.add_options()
        ("help,h", "produce help message")
        ("version,V", "display version information")
        ("config,c",
         boost::program_options::value<std::string>(&opt_config_file_name),
         "name of a file of a configuration.");

    boost::program_options::options_description nameservice_options("CORBA Name Service options");
    std::string nameservice_host;
    unsigned int nameservice_port;
    std::string nameservice_context;
    std::string nameservice_orb_args;
    std::string nameservice_service_logger;
    std::string nameservice_service_logger_request_count;
    nameservice_options.add_options()
            ("nameservice.host", boost::program_options::value<std::string>(&nameservice_host)->default_value(std::string("localhost")), "CORBA name service host name")
            ("nameservice.port", boost::program_options::value<unsigned int>(&nameservice_port)->default_value(2809), "CORBA name service port number")
            ("nameservice.context", boost::program_options::value<std::string>(&nameservice_context)->default_value(std::string("fred")), "context in CORBA name service")
            ("nameservice.orb_args", boost::program_options::value<std::string>(&nameservice_orb_args)->default_value(std::string("-ORBnativeCharCodeSet UTF-8 -ORBendPoint giop:tcp::")), "arguments for CORBA ORB")
            ("nameservice.service_logger", boost::program_options::value<std::string>(&nameservice_service_logger)->default_value(std::string("Logger")), "name of the Logger service");

    boost::program_options::options_description database_options("Database options");
    std::string database_host;
    std::string database_host_addr;
    int database_port;
    std::string database_user;
    std::string database_dbname;
    std::string database_password;
    int database_timeout;
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>(&database_host)->default_value(std::string{"localhost"}), "name of host to connect to")
        ("database.host_addr", boost::program_options::value<std::string>(&database_host_addr), "ip address of host to connect to")
        ("database.port", boost::program_options::value<int>(&database_port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>(&database_user)->default_value("fred"), "PostgreSQL database user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>(&database_dbname)->default_value("fred"), "PostgreSQL database name to connect to")
        ("database.password", boost::program_options::value<std::string>(&database_password), "password used for password authentication")
        ("database.timeout", boost::program_options::value<int>(&database_timeout), "database connection timeout");

    boost::program_options::options_description log_options("Logging options");
    std::string log_device;
    std::string log_default_min_severity;
    log_options.add_options()
        ("log.device", boost::program_options::value<std::string>(&log_device), "where to log (console/file/syslog)")
        ("log.min_severity", boost::program_options::value<std::string>(&log_default_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_console_options("Logging on console options");
    std::string log_console_min_severity;
    log_console_options.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>(&log_console_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_file_options("Logging into file options");
    std::string log_file_name;
    std::string log_file_min_severity;
    log_file_options.add_options()
        ("log.file.file_name",
         boost::program_options::value<std::string>(&log_file_name),
         "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>(&log_file_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_syslog_options("Logging into syslog options");
    boost::optional<int> log_syslog_facility;
    std::string log_syslog_min_severity;

    const auto set_facility = [&](int facility) { 
        if ((facility < 0) || (7 < facility))
        {
            throw Exception("facility out of range [0, 7]");
        }
        log_syslog_facility = facility;
    };
    const auto set_min_severity = [&](const std::string& severity) {
        log_syslog_min_severity = severity;
    };
    log_syslog_options.add_options()
        ("log.syslog.facility",
         boost::program_options::value<int>()->notifier(set_facility),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");

    boost::program_options::options_description logger_options("Server options");
    std::string logger_monitoring_hosts_file;
    std::size_t log_entry_cache_size_max;
    std::size_t session_cache_size_max;
    logger_options.add_options()
        ("logger.monitoring_hosts_file", boost::program_options::value<std::string>(&logger_monitoring_hosts_file), "monitoring hosts file.");
    logger_options.add_options()
        ("logger.log_entry_cache_size_max", boost::program_options::value<std::size_t>(&log_entry_cache_size_max), "maximum number of request idents to be cached");
    logger_options.add_options()
        ("logger.session_cache_size_max", boost::program_options::value<std::size_t>(&session_cache_size_max), "maximum number of session idents to be cached");

    boost::program_options::options_description command_line_options("fred-logger-corba options");
    command_line_options
        .add(command_line_only_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(nameservice_options)
        .add(database_options)
        .add(logger_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(nameservice_options)
        .add(database_options)
        .add(logger_options);

    boost::program_options::variables_map variables_map;
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        throw UnknownOption{out.str()};
    }
    boost::program_options::notify(variables_map);

    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        throw AllDone{out.str()};
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Server Version: " << PACKAGE_VERSION << std::endl;
        out << "LibLogger Version: " << LIBLOGGER_VERSION << std::endl;
        //out << "LibLog Version: " << LIBLOG_VERSION << std::endl;
        //out << "LibPg Version: " << LIBPG_VERSION << std::endl;
        throw AllDone{out.str()};
    }
    const bool config_file_name_presents = has_option("config");
    if (config_file_name_presents)
    {
        this->config_file_name = opt_config_file_name;
        std::ifstream config_file(opt_config_file_name);
        if (!config_file)
        {
            throw Exception{"can not open config file \"" + opt_config_file_name + "\""};
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            throw UnknownOption{out.str()};
        }
        boost::program_options::notify(variables_map);
    }

    if (has_option("nameservice.host"))
    {
        this->nameservice.host = nameservice_host;
    }
    if (has_option("nameservice.port"))
    {
        this->nameservice.port = nameservice_port;
    }
    if (has_option("nameservice.context"))
    {
        this->nameservice.context = nameservice_context;
    }
    if (has_option("nameservice.orb_args"))
    {
        this->nameservice.orb = Fred::Logger::Args(nameservice_orb_args);
    }
    if (has_option("nameservice.service_logger"))
    {
        this->nameservice.service_logger = nameservice_service_logger;
    }
    if (has_option("nameservice.service_logger_request_count"))
    {
        this->nameservice.service_logger_request_count = nameservice_service_logger_request_count;
    }

    this->database.host = database_host;
    if (has_option("database.host_addr"))
    {
        this->database.host_addr = database_host_addr;
    }
    if (has_option("database.port"))
    {
        this->database.port = database_port;
    }
    this->database.user = database_user;
    this->database.dbname = database_dbname;
    if (has_option("database.password"))
    {
        this->database.password = database_password;
    }
    if (has_option("database.timeout"))
    {
        this->database.timeout = database_timeout;
    }

    if (has_option("log.device"))
    {
        boost::optional<LibLog::Level> log_min_severity;
        if (has_option("log.min_severity"))
        {
            log_min_severity = make_severity(log_default_min_severity);
        }
        if (log_device == "console")
        {
            required_one_of(variables_map, "log.min_severity", "log.console.min_severity");
            Log::Console console;
            if (has_option("log.console.min_severity"))
            {
                console.min_severity = make_severity(log_console_min_severity);
            }
            this->log.device = console;
        }
        else if (log_device == "file")
        {
            required_one_of(variables_map, "log.file.file_name");
            required_one_of(variables_map, "log.min_severity", "log.file.min_severity");
            Log::Logfile log_file;
            log_file.file_name = log_file_name;
            if (has_option("log.file.min_severity"))
            {
                log_file.min_severity = make_severity(log_file_min_severity);
            }
            this->log.device = log_file;
        }
        else if (log_device == "syslog")
        {
            required_one_of(variables_map, "log.min_severity", "log.syslog.min_severity");
            Log::Syslog log_syslog;

            static constexpr int default_facility = LOG_USER;
            static const auto make_facility = [](int offset)
            {
                switch (offset)
                {
                    case 0: return LOG_LOCAL0;
                    case 1: return LOG_LOCAL1;
                    case 2: return LOG_LOCAL2;
                    case 3: return LOG_LOCAL3;
                    case 4: return LOG_LOCAL4;
                    case 5: return LOG_LOCAL5;
                    case 6: return LOG_LOCAL6;
                    case 7: return LOG_LOCAL7;
                }
                return default_facility;
            };
            log_syslog.facility = log_syslog_facility != boost::none ? make_facility(*log_syslog_facility)
                                                                     : default_facility;

            if (0 < variables_map.count("log.syslog.min_severity"))
            {
                log_syslog.min_severity = make_severity(log_syslog_min_severity);
            }
            this->log.device = log_syslog;
        }
        else
        {
            throw Exception{"Invalid value of log.device"};
        }
    }

    required_one_of(variables_map, "logger.monitoring_hosts_file");
    this->logger.monitoring_hosts_file = logger_monitoring_hosts_file;
    this->logger.monitoring_hosts_ips = get_monitoring_ips_from_file(this->logger.monitoring_hosts_file);
    required_one_of(variables_map, "logger.log_entry_cache_size_max");
    this->logger.log_entry_cache_size_max = log_entry_cache_size_max;
    required_one_of(variables_map, "logger.session_cache_size_max");
    this->logger.session_cache_size_max = session_cache_size_max;
}

AllDone::AllDone(const std::string& msg)
    : msg_(msg)
{
}

const char* AllDone::what()const noexcept
{
    return msg_.c_str();
}

Exception::Exception(const std::string& msg)
    : std::runtime_error(msg)
{
}

UnknownOption::UnknownOption(const std::string& msg)
    : Exception(msg)
{
}

MissingOption::MissingOption(const std::string& msg)
    : Exception(msg)
{
}

} // namespace Cfg
