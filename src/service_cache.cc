/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/service_cache.hh"

#include "libpg/pg_transaction.hh"
#include "libpg/query.hh"

#include "liblog/liblog.hh"

namespace Fred {
namespace Logger {

template<> template<>
void ServiceCache::reload(
        const LibPg::PgTransaction& _transaction)
{
    LIBLOG_DEBUG("services (re)loaded");
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LibLogger::LogEntry::ServiceName>() << "name" <<
                        LibPg::item<LibLogger::LogEntry::ServiceId>() << "id "
              "FROM service";
            // clang-format on

    const auto db_result = exec(_transaction, query);

    items_.by_u.clear();
    items_.by_v.clear();
    for (LibPg::RowIndex idx = LibPg::RowIndex{0}; idx < db_result.size(); ++idx)
    {
        items_.by_u[db_result[idx].get<LibLogger::LogEntry::ServiceId>()] = db_result[idx].get<LibLogger::LogEntry::ServiceName>(); 
        items_.by_v[db_result[idx].get<LibLogger::LogEntry::ServiceName>()] = db_result[idx].get<LibLogger::LogEntry::ServiceId>(); 
    }
}

} // namespace Fred::Logger
} // namespace Fred
