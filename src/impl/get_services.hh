/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_SERVICES_HH_9B97FA99807F4210AF58A8FA6EC83B0D
#define GET_SERVICES_HH_9B97FA99807F4210AF58A8FA6EC83B0D

#include "liblogger/get_services.hh"

#include <tuple>

namespace Fred {
namespace Logger {
namespace Impl {

struct GetServicesReply
{
    std::vector<std::tuple<LibLogger::LogEntry::ServiceName, LibLogger::LogEntry::ServiceId>> log_entry_services;
};

GetServicesReply get_services();

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
