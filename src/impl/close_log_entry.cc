/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/close_log_entry.hh"

#include "src/get_dsn.hh"
#include "src/log_entry_cache.hh"
#include "src/util.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Logger {
namespace Impl {

void close_log_entry(
        const LibLogger::LogEntry::LogEntryId& _log_entry_id,
        const boost::optional<LibLogger::LogEntry::Content>& _log_entry_content,
        const std::vector<LibLogger::LogEntryProperty>& _log_entry_properties,
        const std::vector<LibLogger::ObjectReferences>& _object_references,
        const boost::variant<LibLogger::LogEntry::ResultCode, LibLogger::LogEntry::ResultName>& _result_code_or_name,
        const boost::optional<LibLogger::LogEntry::SessionId>& _session_id)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};

    const auto log_entry_ident = get_log_entry_ident_by_id(rw_transaction, _log_entry_id);

    const auto session_ident =
            _session_id != boost::none
                    ? boost::optional<LibLogger::SessionIdent>{get_session_ident_by_id(rw_transaction, *_session_id)}
                    : boost::none;

    LibLogger::close_log_entry(
            rw_transaction,
            log_entry_ident,
            _log_entry_content,
            _log_entry_properties,
            _object_references,
            _result_code_or_name,
            session_ident);

    commit(std::move(rw_transaction));

    LogEntryCache::get_instance().erase(_log_entry_id);
}

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred
