/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/get_request_count_users.hh"

#include "src/exceptions.hh"
#include "src/util.hh"
#include "src/get_dsn.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/query.hh"
#include "libpg/util/strong_type.hh"
#include "libpg/util/strong_type_operators.hh"

#include "liblog/liblog.hh"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <deque>
#include <stdexcept>
#include <utility>

namespace Fred {
namespace Logger {
namespace Impl {

namespace {

using RequestPropertyName = LibPg::Util::StrongType<std::string, struct RequestPropertyNameTag_>;
using RequestPropertyId = LibPg::Util::StrongType<int, struct RequestPropertyIdTag_>;

RequestPropertyId get_request_property_id_by_name(
        const LibPg::PgTransaction& _transaction,
        const RequestPropertyName _request_property_name)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<RequestPropertyId>() << "id "
              "FROM request_property_name "
             "WHERE name = " << LibPg::parameter<RequestPropertyName>().as_text();
            // clang-format on

    const auto db_result = exec(
            _transaction,
            query,
            _request_property_name);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("request property name \"{}\" not found", _request_property_name.get());
        throw RequestPropertyNameNotFound{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    return db_result.front().get<RequestPropertyId>();
}

class DailyBorders
{
public:
    DailyBorders(
            const RequestCountFrom& _from,
            const RequestCountTo& _to)
    {
        if (_to.get() < _from.get())
        {
            throw InvalidInterval{};
        }

        borders_.push_back(_from.get());

        auto datetime_to_midnight = RequestCountTo{boost::posix_time::ptime(_from.get().date() + boost::gregorian::days(1))};
        while (datetime_to_midnight < _to)
        {
            borders_.push_back(datetime_to_midnight.get());
            datetime_to_midnight = RequestCountTo{datetime_to_midnight.get() + boost::gregorian::days(1)};
        }
        borders_.push_back(_to.get());
    }

    boost::optional<std::pair<RequestCountFrom, RequestCountTo>> get_next()
    {
        if (borders_.size() > 1)
        {
            const auto from = RequestCountFrom{borders_.front()};
            borders_.pop_front();
            const auto to = RequestCountTo{borders_.front()};
            return std::make_pair(from, to);
        }
        return boost::none;
    }

private:
    std::deque<boost::posix_time::ptime> borders_;
};

class RequestCountAccumulator
{
public:
    RequestCountAccumulator(
            const LibPg::PgTransaction& _ro_transaction,
            const LibLogger::LogEntry::ServiceName& _service_name)
        try : request_property_id_{get_request_property_id_by_name(_ro_transaction, RequestPropertyName{"handle"})},
              service_id_{get_service_id_by_name(_ro_transaction, _service_name)}
    {
    }
    catch (const ItemNotFound&)
    {
        throw InvalidService{};
    }

    void add(
            const LibPg::PgTransaction& _ro_transaction,
            const RequestCountFrom& _from,
            const RequestCountTo& _to)
    {
        const auto query = LibPg::make_query() <<
                // clang-format off
                "SELECT" << LibPg::item<LibLogger::LogEntry::UserName>() << "COALESCE(r.user_name, '')"
                         << LibPg::item<RequestCount>() << "COUNT(*) "
                  "FROM request r "
                  "LEFT JOIN request_property_value rpv ON rpv.request_id = r.id "
                   "AND rpv.request_service_id = " << LibPg::parameter<LibLogger::LogEntry::ServiceId>().as_integer() << " "
                   "AND rpv.request_time_begin >= " << LibPg::parameter<RequestCountFrom>().as_timestamp() << " "
                   "AND rpv.request_time_begin < " << LibPg::parameter<RequestCountTo>().as_timestamp() << " "
                   "AND NOT rpv.request_monitoring "
                   "AND rpv.property_name_id = " << LibPg::parameter<RequestPropertyId>().as_integer() << " "
                  "JOIN result_code rc ON rc.id = r.result_code_id "
                   "AND rc.name NOT IN ('CommandFailed', 'CommandFailedServerClosingConnection') "
                  "JOIN request_type rt ON rt.id = r.request_type_id "
                   "AND rt.name NOT IN ('PollAcknowledgement', 'PollResponse') "
                 "WHERE r.service_id = " << LibPg::parameter<LibLogger::LogEntry::ServiceId>().as_integer() << " "
                   "AND r.time_begin >= " << LibPg::parameter<RequestCountFrom>().as_timestamp() << " "
                   "AND r.time_begin < " << LibPg::parameter<RequestCountTo>().as_timestamp() << " "
                   "AND NOT r.is_monitoring "
                 "GROUP BY r.user_name "
                 "ORDER BY r.user_name";
                // clang-format on

        const auto db_result =
                exec(_ro_transaction, query, {service_id_, _from, _to, request_property_id_});

        for (const auto& row : db_result)
        {
            const auto user_name = row.get<LibLogger::LogEntry::UserName>();
            request_count_info_[user_name] =
                    RequestCount{request_count_info_[user_name].get() + row.get<RequestCount>().get()};
        }
    }

    RequestCountInfo get_request_count_info()
    {
        return request_count_info_;
    }

private:
    const RequestPropertyId request_property_id_;
    const LibLogger::LogEntry::ServiceId service_id_;
    RequestCountInfo request_count_info_;
};

} // namespace Fred::Logger::Impl::{anonymous}

RequestCountInfo get_request_count_users(
        const RequestCountFrom& _request_count_from,
        const RequestCountTo& _request_count_to,
        const LibLogger::LogEntry::ServiceName& _service_name)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};

    auto accumulator = RequestCountAccumulator(ro_transaction, _service_name);

    auto borders = DailyBorders(_request_count_from, _request_count_to);
    while (const auto border = borders.get_next())
    {
        accumulator.add(ro_transaction, border->first, border->second);
    }

    return accumulator.get_request_count_info();
}

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred
