/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/get_log_entry_types.hh"

#include "src/get_dsn.hh"
#include "src/util.hh"

#include "libpg/pg_ro_transaction.hh"

namespace Fred {
namespace Logger {
namespace Impl {

GetLogEntryTypesReply get_log_entry_types(
        const LibLogger::LogEntry::ServiceId& _service_id)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};

    const auto service_name =
            get_service_name_by_id(
                    ro_transaction,
                    _service_id);

    auto result =
            LibLogger::get_log_entry_types(
                    ro_transaction,
                    service_name);

    GetLogEntryTypesReply reply;
    reply.log_entry_types.reserve(result.log_entry_types.size());
    for (const auto& log_entry_type_name : result.log_entry_types)
    {
        reply.log_entry_types.emplace_back(log_entry_type_name, get_log_entry_type_id_by_name(ro_transaction, log_entry_type_name)); 
    }

    commit(std::move(ro_transaction));

    return reply;
}

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred
