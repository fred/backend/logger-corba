/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GET_REQUEST_COUNT_USERS_HH_5D139D4E9FE74A6BA4E1E84CBDED301B
#define GET_REQUEST_COUNT_USERS_HH_5D139D4E9FE74A6BA4E1E84CBDED301B

#include "liblogger/log_entry.hh"

#include "libpg/util/strong_type.hh"
#include "libpg/util/strong_type_operators.hh"

#include <map>

namespace Fred {
namespace Logger {
namespace Impl {

using RequestCountFrom = LibPg::Util::StrongType<boost::posix_time::ptime, struct RequestCountBeginTag_>;
using RequestCountTo = LibPg::Util::StrongType<boost::posix_time::ptime, struct RequestCountEndTag_>;

using RequestCount = LibPg::Util::StrongType<unsigned long long, struct RequestCountTag_>;
using RequestCountInfo = std::map<LibLogger::LogEntry::UserName, RequestCount>;

RequestCountInfo get_request_count_users(
        const RequestCountFrom& request_count_from,
        const RequestCountTo& request_count_to,
        const LibLogger::LogEntry::ServiceName& service);

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
