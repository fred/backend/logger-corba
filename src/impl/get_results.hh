/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_RESULTS_HH_061CFE1472564FFC98549489844EDAF1
#define GET_RESULTS_HH_061CFE1472564FFC98549489844EDAF1

#include "liblogger/get_results.hh"

namespace Fred {
namespace Logger {
namespace Impl {

LibLogger::GetResultsReply get_results(
        const LibLogger::LogEntry::ServiceId& _service_id);

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
