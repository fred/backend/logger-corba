/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_SESSION_HH_A4AA7FFB773B4C339EFB4B5663D9BE1C
#define CREATE_SESSION_HH_A4AA7FFB773B4C339EFB4B5663D9BE1C

#include "liblogger/create_session.hh"

namespace Fred {
namespace Logger {
namespace Impl {

LibLogger::CreateSessionReply create_session(
        const LibLogger::LogEntry::UserName& _user_name,
        const boost::optional<LibLogger::LogEntry::UserId>& _user_id);

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
