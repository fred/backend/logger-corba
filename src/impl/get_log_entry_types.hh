/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_LOG_ENTRY_TYPES_HH_C73BB7EB150C46ABA2E6C8C6C9FEDBB7
#define GET_LOG_ENTRY_TYPES_HH_C73BB7EB150C46ABA2E6C8C6C9FEDBB7

#include "liblogger/get_log_entry_types.hh"

namespace Fred {
namespace Logger {
namespace Impl {

struct GetLogEntryTypesReply
{
    std::vector<std::tuple<LibLogger::LogEntry::LogEntryTypeName, LibLogger::LogEntry::LogEntryTypeId>> log_entry_types;
};

GetLogEntryTypesReply get_log_entry_types(
        const LibLogger::LogEntry::ServiceId& _service_id);

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
