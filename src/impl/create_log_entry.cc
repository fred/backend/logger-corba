/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/create_log_entry.hh"

#include "src/get_dsn.hh"
#include "src/is_monitoring_ip.hh"
#include "src/log_entry_cache.hh"
#include "src/util.hh"

#include "liblogger/session_ident.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Logger {
namespace Impl {

LibLogger::CreateLogEntryReply create_log_entry(
        const boost::optional<LibLogger::LogEntry::SourceIp>& _ip,
        const LibLogger::LogEntry::ServiceId& _log_entry_service_id,
        const boost::optional<LibLogger::LogEntry::Content>& _log_entry_content,
        const std::vector<LibLogger::LogEntryProperty>& _log_entry_properties,
        const std::vector<LibLogger::ObjectReferences>& _object_references,
        const LibLogger::LogEntry::LogEntryTypeId& _log_entry_type_id,
        const boost::optional<LibLogger::LogEntry::SessionId>& _session_id)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};

    const auto log_entry_service_name = get_service_name_by_id(rw_transaction, _log_entry_service_id);

    const auto monitoring_status =
            _ip != boost::none
                    ? LibLogger::LogEntry::IsMonitoring{is_monitoring_ip(_ip->get())}
                    : LibLogger::LogEntry::IsMonitoring{false};

    const auto session_ident =
            _session_id != boost::none
                    ? boost::optional<LibLogger::SessionIdent>{get_session_ident_by_id(rw_transaction, *_session_id)}
                    : boost::none;

    const auto log_entry_type_name = get_log_entry_type_name_by_id(rw_transaction, _log_entry_type_id);

    auto result =
            LibLogger::create_log_entry(
                    rw_transaction,
                    _ip,
                    monitoring_status,
                    log_entry_service_name,
                    _log_entry_content,
                    _log_entry_properties,
                    _object_references,
                    log_entry_type_name,
                    session_ident);

    commit(std::move(rw_transaction));

    LogEntryCache::get_instance().push(
            result.log_entry_ident.id,
            std::make_tuple(result.log_entry_ident.time_begin, result.log_entry_ident.service_handle, result.log_entry_ident.monitoring_status));

    return result;
}

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred
