/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLOSE_LOG_ENTRY_HH_A5A8AC20B645477494881D192C3CD772
#define CLOSE_LOG_ENTRY_HH_A5A8AC20B645477494881D192C3CD772

#include "liblogger/close_log_entry.hh"

namespace Fred {
namespace Logger {
namespace Impl {

void close_log_entry(
        const LibLogger::LogEntry::LogEntryId& _log_entry_id,
        const boost::optional<LibLogger::LogEntry::Content>& _log_entry_content,
        const std::vector<LibLogger::LogEntryProperty>& _log_entry_properties,
        const std::vector<LibLogger::ObjectReferences>& _object_references,
        const boost::variant<LibLogger::LogEntry::ResultCode, LibLogger::LogEntry::ResultName>& _result_code_or_name,
        const boost::optional<LibLogger::LogEntry::SessionId>& _session_id);

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred

#endif
