/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/create_session.hh"

#include "src/get_dsn.hh"
#include "src/session_cache.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Logger {
namespace Impl {

LibLogger::CreateSessionReply create_session(
        const LibLogger::LogEntry::UserName& _user_name,
        const boost::optional<LibLogger::LogEntry::UserId>& _user_id)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};

    auto result =
            LibLogger::create_session(
                    rw_transaction,
                    _user_name,
                    _user_id);

    commit(std::move(rw_transaction));

    SessionCache::get_instance().push(result.session_ident.id, result.session_ident.login_date);

    return result;
}

} // namespace Fred::Logger::Impl
} // namespace Fred::Logger
} // namespace Fred
