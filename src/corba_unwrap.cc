/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/corba_unwrap.hh"

#include "src/exceptions.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <map>
#include <vector>

namespace Fred {
namespace Logger {

namespace {

struct IntegerConversionFailed : std::bad_cast
{
    const char* what() const noexcept
    {
        return "integer conversion failed";
    }
};

template <typename SOURCE_INTEGRAL_TYPE, typename TARGET_INTEGRAL_TYPE>
void int_to_int(SOURCE_INTEGRAL_TYPE _src, TARGET_INTEGRAL_TYPE& _dst)
{
    typedef boost::integer_traits<SOURCE_INTEGRAL_TYPE> source_integral_type_traits;
    typedef boost::integer_traits<TARGET_INTEGRAL_TYPE> target_integral_type_traits;

    BOOST_MPL_ASSERT_MSG(source_integral_type_traits::is_integral, source_type_has_to_be_integral, (SOURCE_INTEGRAL_TYPE));
    BOOST_MPL_ASSERT_MSG(target_integral_type_traits::is_integral, target_type_has_to_be_integral, (TARGET_INTEGRAL_TYPE));
    try
    {
        _dst = boost::numeric_cast<TARGET_INTEGRAL_TYPE>(_src);
    }
    catch (const boost::bad_numeric_cast&)
    {
        throw IntegerConversionFailed{};
    }
}

template <typename TARGET_INTEGRAL_TYPE, typename SOURCE_INTEGRAL_TYPE>
TARGET_INTEGRAL_TYPE corba_unwrap_integral(SOURCE_INTEGRAL_TYPE _src)
{
    typename TARGET_INTEGRAL_TYPE::UnderlyingType _dst;
    int_to_int(_src, _dst);
    return TARGET_INTEGRAL_TYPE{_dst};
}

template<typename T>
decltype(auto) corba_unwrap_const_char(const char* _src)
{
    if (_src == nullptr)
    {
        throw std::runtime_error("corba_unwrap: _src is null");
    }
    return T{_src};
}

} // namespace Fred::Logger::{anonymous}

// createRequest

template<>
boost::optional<LibLogger::LogEntry::SourceIp> corba_unwrap<boost::optional<LibLogger::LogEntry::SourceIp>>(const char* _src)
{
    if ((_src == nullptr) || (_src[0] == '\0'))
    {
        return boost::none;
    }
    boost::system::error_code boost_error_code;
    const auto ip =
            LibLogger::LogEntry::SourceIp{
                    boost::asio::ip::address::from_string(_src, boost_error_code)};
    if (boost_error_code)
    {
        throw InvalidIpAddress{};
    }
    return ip;
}

template<>
LibLogger::LogEntry::Content corba_unwrap<LibLogger::LogEntry::Content>(const char* _src)
{
    return corba_unwrap_const_char<LibLogger::LogEntry::Content>(_src);
}

LibLogger::LogEntry::ServiceId corba_unwrap(const ccReg::RequestServiceType& _src)
{
    return corba_unwrap_integral<LibLogger::LogEntry::ServiceId>(_src);
}

namespace {

template <typename T>
T corba_unwrap(const CORBA::String_member& _src);

template<>
LibLogger::LogEntry::PropertyType corba_unwrap<LibLogger::LogEntry::PropertyType>(const CORBA::String_member& _src)
{
    return LibLogger::LogEntry::PropertyType{_src.in()};
}

template<>
LibLogger::LogEntry::PropertyValue corba_unwrap<LibLogger::LogEntry::PropertyValue>(const CORBA::String_member& _src)
{
    return LibLogger::LogEntry::PropertyValue{_src.in()};
}

template<>
LibLogger::LogEntry::LogEntryObjectTypeName corba_unwrap<LibLogger::LogEntry::LogEntryObjectTypeName>(const CORBA::String_member& _src)
{
    return LibLogger::LogEntry::LogEntryObjectTypeName{_src.in()};
}

bool corba_unwrap(const CORBA::Boolean& _src)
{
    return _src;
}

} // namespace Fred::Logger::{anonymous}

std::vector<LibLogger::LogEntryProperty> corba_unwrap(const ccReg::RequestProperties& _request_properties)
{
    std::vector<LibLogger::LogEntryProperty> ret;
    std::vector<LibLogger::LogEntryPropertyValue> property_values; // property values of current type
    std::map<LibLogger::LogEntry::PropertyType, std::vector<LibLogger::LogEntry::PropertyValue>> property_children;

    CORBA::ULong idx = 0;
    while (idx < _request_properties.length())
    {
        const auto current_property_type = corba_unwrap<LibLogger::LogEntry::PropertyType>(_request_properties[idx].name);
        const auto current_property_value = corba_unwrap<LibLogger::LogEntry::PropertyValue>(_request_properties[idx].value);
        ++idx;
        if (idx < _request_properties.length())
        {
            while (idx < _request_properties.length() && corba_unwrap(_request_properties[idx].child))
            {
                property_children[corba_unwrap<LibLogger::LogEntry::PropertyType>(_request_properties[idx].name)].push_back(corba_unwrap<LibLogger::LogEntry::PropertyValue>(_request_properties[idx].value));
                ++idx;
            }
            if (idx < _request_properties.length() && corba_unwrap<LibLogger::LogEntry::PropertyType>(_request_properties[idx].name) == current_property_type)
            {
                property_values.emplace_back(
                        current_property_value,
                        property_children);
                property_children.clear();
            }
            else
            {
                property_values.emplace_back(
                        current_property_value,
                        property_children);
                property_children.clear();
                ret.emplace_back(
                        current_property_type,
                        property_values);
                property_values.clear();
            }
        }
        else // single child property
        {
            property_values.emplace_back(
                    current_property_value,
                    property_children);
            ret.emplace_back(
                    current_property_type,
                    property_values);
        }
    }
    return ret;
}

template <>
LibLogger::LogEntry::ObjectIdent corba_unwrap<LibLogger::LogEntry::ObjectIdent>(ccReg::TID _src)
{
    return LibLogger::LogEntry::ObjectIdent{std::to_string(_src)};
}

std::vector<LibLogger::ObjectReferences> corba_unwrap(const ccReg::ObjectReferences& _object_references)
{
    std::vector<LibLogger::ObjectReferences> ret;
    std::vector<LibLogger::LogEntry::ObjectIdent> object_references; // object references of current type

    CORBA::ULong idx = 0;
    while (idx < _object_references.length())
    {
        const auto current_object_type = corba_unwrap<LibLogger::LogEntry::LogEntryObjectTypeName>(_object_references[idx].type);
        const auto current_object_ident = corba_unwrap<LibLogger::LogEntry::ObjectIdent>(_object_references[idx].id);
        object_references.emplace_back(
                current_object_ident);
        ++idx;
        if (idx < _object_references.length())
        {
            const auto next_object_type = corba_unwrap<LibLogger::LogEntry::LogEntryObjectTypeName>(_object_references[idx].type);
            if (next_object_type != current_object_type)
            {
                ret.emplace_back(
                        current_object_type,
                        object_references);
                object_references.clear();
            }
        }
        else {
            ret.emplace_back(
                    current_object_type,
                    object_references);
        }
    }
    return ret;
}

template <>
LibLogger::LogEntry::LogEntryTypeId corba_unwrap<LibLogger::LogEntry::LogEntryTypeId>(CORBA::Long _src)
{
    return corba_unwrap_integral<LibLogger::LogEntry::LogEntryTypeId>(_src);
}

// closeRequest

template <>
LibLogger::LogEntry::LogEntryId corba_unwrap<LibLogger::LogEntry::LogEntryId>(ccReg::TID _src)
{
    return LibLogger::LogEntry::LogEntryId{_src};
}

template <>
LibLogger::LogEntry::ResultCode corba_unwrap<LibLogger::LogEntry::ResultCode>(CORBA::Long _src)
{
    return corba_unwrap_integral<LibLogger::LogEntry::ResultCode>(_src);
}

// createSession

template <>
boost::optional<LibLogger::LogEntry::UserId> corba_unwrap<boost::optional<LibLogger::LogEntry::UserId>>(ccReg::TID _src)
{
    return _src == 0
                   ? boost::optional<LibLogger::LogEntry::UserId>{}
                   : boost::optional<LibLogger::LogEntry::UserId>{_src};
}

template<>
LibLogger::LogEntry::UserName corba_unwrap<LibLogger::LogEntry::UserName>(const char* _src)
{
    return corba_unwrap_const_char<LibLogger::LogEntry::UserName>(_src);
}

// closeSession

template <>
LibLogger::LogEntry::SessionId corba_unwrap<LibLogger::LogEntry::SessionId>(ccReg::TID _src)
{
    return LibLogger::LogEntry::SessionId{_src};
}

template <>
boost::optional<LibLogger::LogEntry::SessionId> corba_unwrap<boost::optional<LibLogger::LogEntry::SessionId>>(ccReg::TID _src)
{
    return _src != 0 ? boost::optional<LibLogger::LogEntry::SessionId>{_src} : boost::none;
}

// getRequestCountUsers

template<>
Impl::RequestCountFrom corba_unwrap<Impl::RequestCountFrom>(const char* _src)
{
    return Impl::RequestCountFrom{boost::posix_time::from_iso_string(corba_unwrap_const_char<std::string>(_src))};
}

template<>
Impl::RequestCountTo corba_unwrap<Impl::RequestCountTo>(const char* _src)
{
    return Impl::RequestCountTo{boost::posix_time::from_iso_string(corba_unwrap_const_char<std::string>(_src))};
}

template<>
LibLogger::LogEntry::ServiceName corba_unwrap<LibLogger::LogEntry::ServiceName>(const char* _src)
{
    return corba_unwrap_const_char<LibLogger::LogEntry::ServiceName>(_src);
}

} // namespace Fred::Logger
} // namespace Fred
