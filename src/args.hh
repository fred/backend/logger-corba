/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGS_HH_AF4DA63D2ED346D3A7797B92B4FE8E8B
#define ARGS_HH_AF4DA63D2ED346D3A7797B92B4FE8E8B

#include <string>
#include <vector>

namespace Fred {
namespace Logger {

class Args
{
public:
    using Argc = int;
    using Argv = char**;

    Args();
    Args(const std::string& _src);
    Args(const Args& _args);
    Args& operator=(Args _args);
    int argc() const;
    char** argv() const;

private:
    std::vector<std::vector<char>> argv_data_;
    mutable std::vector<char*> argv_;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
