/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/args.hh"

#include <algorithm>
#include <cstdlib>
#include <iterator>
#include <iostream>
#include <regex>
#include <vector>

namespace Fred {
namespace Logger {

Args::Args()
{
    argv_.push_back(nullptr);
}

Args::Args(const std::string& _src)
{
    const auto delimiter = std::regex("\\s+");
    static constexpr const auto unused_argv_0 = '\0';
    argv_data_.push_back(std::vector<char>{unused_argv_0});

    if (!_src.empty())
    {
        constexpr auto stuff_between_matches = -1;
        const std::sregex_token_iterator begin(_src.cbegin(), _src.cend(), delimiter, stuff_between_matches);
        static const std::sregex_token_iterator end;
        std::transform(begin, end, std::back_inserter(argv_data_),
                                   [](const std::string& token) -> decltype(auto)
                                   {
                                       return std::vector<char>{token.c_str(), token.c_str() + token.length() + 1};
                                   });
    }
    argv_.reserve(argv_data_.size() + 1);
    for (auto& arg : argv_data_)
    {
        argv_.push_back(arg.data());
    }
    argv_.push_back(nullptr);
}

Args::Args(const Args& _args)
    : argv_data_(_args.argv_data_)
{
    for (auto& arg : argv_data_)
    {
        argv_.push_back(arg.data());
    }
    argv_.push_back(nullptr);
}

Args& Args::operator=(Args _args)
{
    argv_data_ = std::move(_args.argv_data_);
    argv_ = std::move(_args.argv_);
    return *this;
}

int Args::argc() const
{
    return argv_.size() - 1;
}

char** Args::argv() const
{
    return argv_.data();
}

} // namespace Fred::Logger
} // namespace Fred
