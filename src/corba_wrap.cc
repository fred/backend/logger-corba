/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/corba_wrap.hh"

#include "src/impl/get_request_count_users.hh"

#include <boost/integer_traits.hpp>
#include <boost/numeric/conversion/cast.hpp>

#include <exception>

namespace Fred {
namespace Logger {

namespace {

CORBA::String_var corba_wrap(const std::string& _src)
{
    return CORBA::string_dup(_src.c_str());
}

CORBA::String_var corba_wrap(const LibLogger::LogEntry::ServiceName& _src)
{
    return corba_wrap(_src.get());
}

CORBA::String_var corba_wrap(const LibLogger::LogEntry::LogEntryTypeName& _src)
{
    return corba_wrap(_src.get());
}

struct IntegerConversionFailed : std::bad_cast
{
    const char* what() const noexcept
    {
        return "integer conversion failed";
    }
};

template <typename SOURCE_INTEGRAL_TYPE, typename TARGET_INTEGRAL_TYPE>
void int_to_int(SOURCE_INTEGRAL_TYPE _src, TARGET_INTEGRAL_TYPE& _dst)
{
    typedef boost::integer_traits<SOURCE_INTEGRAL_TYPE> source_integral_type_traits;
    typedef boost::integer_traits<TARGET_INTEGRAL_TYPE> target_integral_type_traits;

    BOOST_MPL_ASSERT_MSG(source_integral_type_traits::is_integral, source_type_has_to_be_integral, (SOURCE_INTEGRAL_TYPE));
    BOOST_MPL_ASSERT_MSG(target_integral_type_traits::is_integral, target_type_has_to_be_integral, (TARGET_INTEGRAL_TYPE));
    try
    {
        _dst = boost::numeric_cast<TARGET_INTEGRAL_TYPE>(_src);
    }
    catch (const boost::bad_numeric_cast&)
    {
        throw IntegerConversionFailed{};
    }
}

template <typename TARGET_INTEGRAL_TYPE, typename SOURCE_INTEGRAL_TYPE>
TARGET_INTEGRAL_TYPE corba_wrap_integral(SOURCE_INTEGRAL_TYPE _src)
{
    TARGET_INTEGRAL_TYPE _dst;
    int_to_int(_src, _dst);
    return _dst;
}

ccReg::RequestServiceType corba_wrap(const LibLogger::LogEntry::ServiceId& _src)
{
    return corba_wrap_integral<ccReg::RequestServiceType>(_src.get());
}

ccReg::RequestServiceType corba_wrap(const LibLogger::LogEntry::LogEntryTypeId& _src)
{
    return corba_wrap_integral<ccReg::RequestServiceType>(_src.get());
}

ccReg::TID corba_wrap(const LibLogger::LogEntry::SessionId& _src)
{
    return corba_wrap_integral<ccReg::TID>(_src.get());
}

ccReg::TID corba_wrap(const LibLogger::LogEntry::LogEntryId& _src)
{
    return corba_wrap_integral<ccReg::TID>(_src.get());
}

} // namespace Fred::Logger::{anonymous}

// createRequest
ccReg::TID corba_wrap(const LibLogger::CreateLogEntryReply& _reply)
{
    return corba_wrap(_reply.log_entry_ident.id);
}

// createSession
ccReg::TID corba_wrap(const LibLogger::CreateSessionReply& _reply)
{
    return corba_wrap(_reply.session_ident.id);
}

// getRequestTypesByService
ccReg::RequestTypeList* corba_wrap(const Impl::GetLogEntryTypesReply& _reply)
{
    ccReg::RequestTypeList_var ret = new ccReg::RequestTypeList{};

    const auto items = _reply.log_entry_types.size();

    ret->length(items);
    for (std::size_t idx = 0; idx < items; ++idx)
    {
        ret[idx].id = corba_wrap(std::get<LibLogger::LogEntry::LogEntryTypeId>(_reply.log_entry_types.at(idx)));
        ret[idx].name = corba_wrap(std::get<LibLogger::LogEntry::LogEntryTypeName>(_reply.log_entry_types.at(idx)));
    }

    return ret._retn();
}

// getServices
ccReg::RequestServiceList* corba_wrap(const Impl::GetServicesReply& _reply)
{
    ccReg::RequestServiceList_var ret = new ccReg::RequestServiceList{};

    const auto items = _reply.log_entry_services.size();

    ret->length(items);
    for (std::size_t idx = 0; idx < items; ++idx)
    {
        ret[idx].id = corba_wrap(std::get<LibLogger::LogEntry::ServiceId>(_reply.log_entry_services.at(idx)));
        ret[idx].name = corba_wrap(std::get<LibLogger::LogEntry::ServiceName>(_reply.log_entry_services.at(idx)));
    }

    return ret._retn();
}

namespace {

CORBA::String_var corba_wrap(const LibLogger::LogEntry::ResultName& _result_name)
{
    return corba_wrap(_result_name.get());
}

CORBA::ULong corba_wrap(const LibLogger::LogEntry::ResultCode& _result_code)
{
    return corba_wrap_integral<CORBA::ULong>(_result_code.get());
}

} // Fred::Logger::{anonymous}

// getResultCodesByService
ccReg::ResultCodeList* corba_wrap(const LibLogger::GetResultsReply& _reply)
{
    ccReg::ResultCodeList_var ret = new ccReg::ResultCodeList{};

    const auto items = _reply.result_code_map.size();

    ret->length(items);
    CORBA::ULong idx = 0;
    for (const auto& result_code : _reply.result_code_map)
    {
        ret[idx].result_code = corba_wrap(result_code.second);
        ret[idx].name = corba_wrap(result_code.first);
        ++idx;
    }
    return ret._retn();
}

// getObjectTypes
ccReg::Logger::ObjectTypeList* corba_wrap(const LibLogger::GetObjectReferenceTypesReply& _reply)
{
    ccReg::Logger::ObjectTypeList_var ret = new ccReg::Logger::ObjectTypeList{};

    const auto items = _reply.object_reference_types.size();

    ret->length(items);
    for (std::size_t idx = 0; idx < items; ++idx)
    {
        ret[idx] = corba_wrap(_reply.object_reference_types.at(idx).get());
    }

    return ret._retn();
}

// getRequestCountUsers

namespace {

CORBA::String_var corba_wrap(const LibLogger::LogEntry::UserName& _src)
{
    return corba_wrap(_src.get());
}

} // namespace Fred::Logger::{anonymous}

ccReg::RequestCountInfo* corba_wrap(const Impl::RequestCountInfo& _reply)
{
    ccReg::RequestCountInfo_var ret = new ccReg::RequestCountInfo{};

    const auto items = _reply.size();

    ret->length(items);
    CORBA::ULong idx = 0;
    for (const auto& item : _reply)
    {
        ret[idx].user_handle = corba_wrap(item.first);
        ret[idx].count       = corba_wrap_integral<CORBA::ULong>(item.second.get());
        ++idx;
    }
    return ret._retn();
}

} // namespace Fred::Logger
} // namespace Fred
