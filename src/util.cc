/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util.hh"

#include "src/log_entry_cache.hh"
#include "src/log_entry_type_cache.hh"
#include "src/service_cache.hh"
#include "src/session_cache.hh"

#include "liblogger/exceptions.hh"

#include "libpg/pg_result.hh"
#include "libpg/pg_transaction.hh"
#include "libpg/query.hh"
#include "libpg/util/strong_type_operators.hh"

#include "liblog/liblog.hh"

#include <stdexcept>

namespace Fred {
namespace Logger {

LibLogger::SessionIdent get_session_ident_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::SessionId& _session_id)
{
    const auto session_login_date = SessionCache::get_instance().get(_session_id, [&]() -> decltype(auto) { return _transaction; });

    return LibLogger::SessionIdent{
            _session_id,
            session_login_date};
}

LibLogger::LogEntryIdent get_log_entry_ident_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryId& _log_entry_id)
{
    const auto log_entry_details = LogEntryCache::get_instance().get(_log_entry_id, [&]() -> decltype(auto) { return _transaction; });

    return LibLogger::LogEntryIdent{
            _log_entry_id,
            std::get<LibLogger::LogEntry::TimeBegin>(log_entry_details),
            std::get<LibLogger::LogEntry::ServiceName>(log_entry_details),
            std::get<LibLogger::LogEntry::IsMonitoring>(log_entry_details)};
}

LibLogger::LogEntry::ServiceName get_service_name_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::ServiceId& _service_id)
{
    return ServiceCache::get_instance().get(_service_id, [&]() -> decltype(auto) { return _transaction; });
}

LibLogger::LogEntry::ServiceId get_service_id_by_name(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::ServiceName& _service_name)
{
    return ServiceCache::get_instance().get(_service_name, [&]() -> decltype(auto) { return _transaction; });
}

LibLogger::LogEntry::LogEntryTypeName get_log_entry_type_name_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryTypeId& _log_entry_type_id)
{
    return LogEntryTypeCache::get_instance().get(_log_entry_type_id, [&]() -> decltype(auto) { return _transaction; });
}

LibLogger::LogEntry::LogEntryTypeId get_log_entry_type_id_by_name(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::LogEntryTypeName& _log_entry_type_name)
{
    return LogEntryTypeCache::get_instance().get(_log_entry_type_name, [&]() -> decltype(auto) { return _transaction; });
}

} // namespace Fred::Logger
} // namespace Fred
