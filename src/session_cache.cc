/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/session_cache.hh"

#include "src/cfg.hh"
#include "src/exceptions.hh"

#include "libpg/query.hh"

#include "liblog/liblog.hh"

namespace Fred {
namespace Logger {

namespace {

LibLogger::LogEntry::SessionLoginDate get_session_login_date_by_id(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::SessionId& _session_id)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LibLogger::LogEntry::SessionLoginDate>() << "login_date "
              "FROM session "
             "WHERE id = " << LibPg::parameter<LibLogger::LogEntry::SessionId>().as_big_int();
            // clang-format on

    const auto db_result = exec(_transaction, query, _session_id);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("session \"{}\" not found", _session_id.get());
        throw SessionDoesNotExist{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto session_login_date = db_result.front().get<LibLogger::LogEntry::SessionLoginDate>();

    return session_login_date;
}

} // namespace Fred::Logger::{anonymous}

template<> template<>
SessionCache::Value SessionCache::get_on_cache_miss(
        const LibPg::PgTransaction& _transaction,
        const LibLogger::LogEntry::SessionId& _session_id)
{
    LIBLOG_DEBUG("partitioning related details of session of id \"{}\" not cached, querying db", _session_id.get());

    const auto session_login_date = get_session_login_date_by_id(_transaction, _session_id);

    return session_login_date;
}

template<>
void SessionCache::erase(const SessionCache::Key& _key) = delete;

} // namespace Fred::Logger
} // namespace Fred
